package org.example;

import java.sql.ResultSet;
import java.sql.SQLException;

public class option_5 {
    public static void execute(sql_class sql) throws SQLException {
        long startTime = System.currentTimeMillis();

        ResultSet rs = sql.get_males();

        long endTime = System.currentTimeMillis();

        System.out.print("Name|");
        System.out.print("Birthday|");
        System.out.println("Gender|\n");
        while (rs.next()) {
            System.out.print(rs.getString(1) + "|");
            System.out.print(rs.getDate(2).toString()  + "|");
            System.out.println(rs.getString(3));
        }
        System.out.println("\nFetching table took " + (endTime - startTime) + " milliseconds");
    }
}
