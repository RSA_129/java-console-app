package org.example;

import java.sql.SQLException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Random;

public class option_4 {
    public static void execute(sql_class sql) throws SQLException {
        String[] names = new String[1000100];
        Date[] birthdays = new Date[1000100];
        String[] genders = new String[1000100];
        for (int i=0;i<1000000;i++) {
            names[i] = get_name();
            birthdays[i] = get_date();
            genders[i] = get_gender();
        }

        for (int i=1000000;i<1000100;i++) {
            String name = get_name();
            name = "F" + name.substring(0, 9);
            names[i] = name;
            birthdays[i] = get_date();
            genders[i] = "M";
        }

        sql.insert_many(names, birthdays, genders);

    }

    private static String get_name() {
        String[] names = new String[10];
        for (int i = 0; i < 10; i++) {
            int r = new Random().nextInt(52);
            char start = (r < 26) ? 'A' : 'a';
            String c = String.valueOf((char)(start + r % 26));
            names[i] = c;
        }
        return String.join("", names);
    }

    private static Date get_date() {
        Random random = new Random();
        int minDay = (int) LocalDate.of(1970, 1, 1).toEpochDay();
        int maxDay = (int) LocalDate.of(2015, 12, 31).toEpochDay();
        long randomDay = minDay + random.nextInt(maxDay - minDay);

        return Date.valueOf(LocalDate.ofEpochDay(randomDay));
    }

    private static String get_gender() {
        int random_int = new Random().nextInt(2);
        String[] choose = {"M", "F"};
        return choose[random_int];

    }
}
