package org.example;

import java.sql.*;

public class sql_class {
    private Connection connection;

    // docker url db
//    final String server_url = "jdbc:mysql://app-db/info?serverTimezone=UTC";
    // local url db
    final String server_url = "jdbc:mysql://localhost:3306/info?serverTimezone=UTC";

    public int connect() {
        try {
            this.connection = DriverManager.getConnection(
                    this.server_url,
                    System.getenv("user"), System.getenv("pass"));
            this.connection.setAutoCommit(false);
            return 0;
        }
        catch (SQLException exception) {
            System.err.println(exception);
            return 1;
        }
    }

    public void create_table() throws SQLException {
        Statement statement = this.connection.createStatement();
        statement.executeUpdate("drop table if exists people_info");
        String query = "CREATE TABLE `people_info` (" +
                "  `id` INT NOT NULL AUTO_INCREMENT," +
                "  `name` varchar(45) NOT NULL," +
                "  `birthday` date NOT NULL," +
                "  `gender` varchar(1) NOT NULL," +
                "  PRIMARY KEY (id)" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;";
        statement.executeUpdate(query);
        statement.close();
    }

    public void create_index() throws Exception {
        Statement statement = this.connection.createStatement();
        String query = "create index name on people_info(name, gender)";
        statement.executeUpdate(query);
        statement.close();
    }

    public boolean table_exists() throws SQLException {
        Statement statement = this.connection.createStatement();
        String query = "show tables like 'people_info'";
        ResultSet rs = statement.executeQuery(query);
        return rs.next();
    }

    public ResultSet get_unique() throws SQLException {
        Statement statement = this.connection.createStatement();
        String query = "SELECT name, birthday, min(gender) as gender, " +
                "timestampdiff(year, birthday, curdate()) as age " +
                "from people_info " +
                "group by name, birthday " +
                "order by name";
        return statement.executeQuery(query);
    }
    public ResultSet get_males() throws SQLException {
        Statement statement = this.connection.createStatement();
        String query = "SELECT name, birthday, gender FROM people_info\n" +
                "where name like 'F%' and gender = 'M'";
        return statement.executeQuery(query);
    }
    public void insert(String name, Date birthday, String gender) throws SQLException {
        String query = "insert into people_info values (NULL,?,?,?)";
        PreparedStatement statement = this.connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setDate(2, birthday);
        statement.setString(3, gender);
        statement.execute();
        this.connection.commit();
    }

    public void insert_many(String[] names, Date[] birthdays, String[] genders) throws SQLException {
        String query = "insert into people_info values (NULL,?,?,?)";
        PreparedStatement statement = this.connection.prepareStatement(query);
        for (int i = 0; i < names.length; i++) {
            statement.setString(1, names[i]);
            statement.setDate(2, birthdays[i]);
            statement.setString(3, genders[i]);
            statement.addBatch();
        }
        System.out.println("Wait until query will be executed");
        statement.executeBatch();
        this.connection.commit();
    }

    public void close() {
        try {
            this.connection.close();
        }
        catch (SQLException exception) {
            System.err.println(exception);
        }
        catch (NullPointerException ignored) {}
    }
}
