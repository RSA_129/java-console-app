package org.example;

import java.sql.ResultSet;
import java.sql.SQLException;

public class option_3 {
    public static void execute(sql_class sql) throws SQLException {
        ResultSet rs = sql.get_unique();
        System.out.print("Name|");
        System.out.print("Birthday|");
        System.out.print("Gender|");
        System.out.println("Age\n");
        while (rs.next()) {
            System.out.print(rs.getString(1) + "|");
            System.out.print(rs.getDate(2).toString()  + "|");
            System.out.print(rs.getString(3) + "|");
            System.out.println(rs.getString(4) + "|");
        }
    }
}
