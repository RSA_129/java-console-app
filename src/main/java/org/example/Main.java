package org.example;

import java.util.Objects;

public class Main {
    public static void main(String[] args) throws Exception {
        sql_class sql = new sql_class();
        if (sql.connect() == 1) {
            System.err.println("Connection to db failed");
            return;
        }
        System.out.println("Connection to db is established");
        if (args.length < 1)
            System.out.println("Run program with arguments");
        else if (Objects.equals(args[0], "1")) {
            option_1.execute(sql);
            System.out.println("Table was created in given database");
            option_1.create_index(sql);
            System.out.println("Index was created in given database");
        }
        else if (!sql.table_exists())
            System.out.println("Table wasn't created");
        else if (Objects.equals(args[0], "2")) {
            int out = option_2.execute(sql, args);
            switch (out) {
                case (0) -> System.out.println("Your row has been inserted");
                case (1) -> System.out.println("You must have 4 total parameters with type '2'");
                case (3) -> System.out.println("Birthday should be in format dd.mm.yyyy");
                case (4) -> System.out.println("Gender should be 'M' or 'F'");
                default -> System.out.println("You should have right type of data be typed correctly");
            }
        }
        else if (Objects.equals(args[0], "3")) {
            System.out.println("Here's output of people:\n");
            option_3.execute(sql);
        }
        else if (Objects.equals(args[0], "4")) {
            option_4.execute(sql);
            System.out.println("Successfully inserted sample data");
        }

        else if (Objects.equals(args[0], "5")) {
            System.out.println("Here's output of people:\n");
            option_5.execute(sql);
        }
        sql.close();
    }
}

