package org.example;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.Arrays;

public class option_2 {
    public static int execute(sql_class sql, String[] args) throws SQLException {

        if (args.length != 4) return 1;

        args = Arrays.copyOfRange(args, 1, 4);
        String name = args[0], birthday_str = args[1], gender = args[2].toUpperCase();

        if (name.length() > 44) return 2;
        if (date_wrong(birthday_str)) return 3;
        LocalDate birthday = LocalDate.parse(birthday_str,
                                DateTimeFormatter.ofPattern( "dd.MM.uuuu"));

        if (!Arrays.asList(new String[]{"M", "F"}).contains(gender)) return 4;
        sql.insert(name, Date.valueOf(birthday), gender);
        return 0;

    }

    private static boolean date_wrong(String data) {
        DateTimeFormatter f = DateTimeFormatter.ofPattern ( "dd.MM.uuuu");
        f = f.withResolverStyle ( ResolverStyle.STRICT );
        try {
            LocalDate ld = LocalDate.parse ( data , f );
            return false;
        } catch ( DateTimeParseException e ) {
            return true;
        }
    }
}
