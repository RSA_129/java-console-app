FROM maven:3.8.6-openjdk-18-slim
WORKDIR /app
COPY target/ ./target
COPY pom.xml .
CMD mvn exec:java -Dexec.mainClass=org.example.Main -Dexec.cleanupDaemonThreads=false -Dexec.args="1"
